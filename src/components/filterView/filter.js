// 日期控件 https://github.com/mengxiong10/vue2-datepicker/blob/master/README.zh-CN.md
import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
import 'vue2-datepicker/locale/zh-cn';
// import './datepicker.scss';
// import 'vue2-datepicker/scss/index.scss';
// import { DatePicker } from 'vue2-datepicker';

export default {
	name: 'filter-view',
	components: {
		DatePicker
	},
	props: {
		conditions: {
			type: Array,
			required: true
		}

	},
	computed: {
		/** 判断row.item...value是否被选中 */

		/**
		 * 判断改行是否还有选中的，包括自定义
		 * @param type 表示row.value，即：项目类型
		 * @returns 
		 * 1：整行都没有被选中的
		 * 2：有被选中的
		 */
		isRowSelected() {
			return (type) => {
				let _rows = this.selected.filter((item) => item.value == type)
				if (_rows == null || _rows.length == 0) {
					// 没有被选中
					return 1
				}
				if (_rows[0].data == undefined || _rows[0].data == null || _rows[0].data.length == 0) {
					return 1
				} else {
					return 2
				}
			}
		},
		/**
		 * @param type 表示row.value，即：项目类型
		 * @param value 表示具体选项的value
		 * @retruns
		 * 1：整行都没有被选中的
		 * 2：当前行设置了自定义数据
		 * 3：当前item被未选中
		 * 4：当前item被选中
		 */
		isSelected() {
			return (type, value) => {
				// 判断该行是否有被选中的 
				let _rows = this.selected.filter((item) => item.value == type)
				if (_rows == null || _rows.length == 0) {
					return 1
				}
				// 判断是否有自定义数据（自定义数据是单选）
				let hasCustom = true // 有自定义数据 { "label": "custom", "value": "1570550400000" }
				let _custom_item = _rows[0].data.filter((item) => item.label == 'custom')
				if (_custom_item == null || _custom_item.length == 0) {
					// 没有自定数据
					hasCustom = false
				} else {
					// 有自定数据
					return 2
				}

				// 如果没有自定义数据，判断其他item是否被选中
				let _item = _rows[0].data.filter((item) => item.value == value)
				if (_item == null || _item.length == 0) {
					return 3
				}
				// 当前item被选中
				return 4;
			}
		}
	},
	methods: {
		// 提交事件
		submit() {
			this.$emit('change', this.selected)
		},
		/** 自定义文本更改 single*/
		singleSubmit(row) {
			let _value = this.singleStringValues[row.value]
			let _custom = { "label": "custom", "value": _value }
			this.setSingle(row, _custom, 'custom')

			this.submit()
		},
		/** 自定义文本更改 range */
		rangeSubmit(row) {
			let _range = this.rangeStringValues[row.value + '___num1'] + '-' + this.rangeStringValues[row.value + '___num2']
			let _custom = { "label": "custom", "value": _range }
			this.setSingle(row, _custom, 'custom')

			this.submit()
		},
		/** 自定义时间更改  single*/
		dateChangeHandler(row, date) {
			let _custom = { "label": "custom", "value": new Date(date).getTime() }
			this.setSingle(row, _custom, 'custom')

			this.submit()
		},
		/** 自定义事件更改 - range */
		dateRangeChangeHandler(row, dates) {
			let _range = new Date(dates[0]).getTime() + '-' + new Date(dates[1]).getTime()
			console.log('时间范围', _range)
			let _custom = { "label": "custom", "value": _range }
			this.setSingle(row, _custom, 'custom')

			this.submit()
		},
		/** 选中全部，即清除selected中对应的对象 */
		clearSelect(rowValue) {
			let _index = this.getRowIndex(rowValue)
			// 删除整行
			if (_index > -1) {
				this.selected.splice(_index, 1)
			}

			this.submit()
		},
		/**选中条件，单选、多选、自定义 */
		selectRowItem(row, item) {
			// 判断是否为多选
			if (row.isMultiple === true) {
				// 多选 
				this.setMultiple(row, item)

			} else if (row.isMultiple === false) {
				// 单选，直接替换掉
				this.setSingle(row, item)
			}

			this.submit()
		},


		/** 多选 */
		setMultiple(row, item) {
			// 判断是否存在
			let _index = this.getRowIndex(row.value)
			if (_index == -1) {
				// 如果整行不存在，则添加整行
				let _row = JSON.parse(JSON.stringify(row))
				_row.data = [item]
				this.selected.push(_row)
				console.log(this.selected)
			} else {
				// 
				// 获取item在选中列表中的位置

				let _itemIndex = this.getItemIndex(this.selected[_index], item)
				// 如果选择项不存在
				if (_itemIndex == -1) {
					// 添加选择项
					// debugger
					let _tempDatas = JSON.parse(JSON.stringify(this.selected[_index].data))
					_tempDatas.push(item)
					this.$set(this.selected[_index], `data`, _tempDatas)
				} else {
					// 如果选择项存在，删除选择项
					let _tempDatas = JSON.parse(JSON.stringify(this.selected[_index].data))
					_tempDatas.splice(_itemIndex, 1)
					this.$set(this.selected[_index], `data`, _tempDatas)
				}

			}
			// 如果存在，则去除

		},
		/** 单选 */
		setSingle(row, item, type) {
			if (type == undefined || type == "item") {
				// 清除自定义

				// 清除single文本
				this.$set(this.singleStringValues, row.value, null)
				// 清除range 文本
				this.$set(this.rangeStringValues, row.value + '___num1', null)
				this.$set(this.rangeStringValues, row.value + '___num2', null)
				// 清除时间
				this.$set(this.datePickerValues, row.value, null)
			} else if (type == 'custom') {
				// 设置为自定义数据

			}
			// 直接替换到row.item.data = []
			let _index = this.getRowIndex(row.value)
			if (_index == -1) {
				// 表示selected里没有对应的row，则：添加整个row进入selected
				let _row = JSON.parse(JSON.stringify(row))
				_row.data = [item]
				this.selected.push(_row)
			} else {
				// 因为是单选，所以替换掉整个data
				let _tempDatas = [item]
				this.$set(this.selected[_index], `data`, _tempDatas)
			}
		},
		/** 
		 * 获取row在selected的下标
		 * @return -1：表示没有
		 */
		getRowIndex(rowValue) {
			let _index = (this.selected || []).findIndex((row) => row.value === rowValue)
			return _index
		},
		/** 
		 * 获取item在row的下标
		 * @return -1：表示没有
		 */
		getItemIndex(row, item) {
			let _index = (row.data || []).findIndex((_item) => _item.value === item.value)
			return _index
		}
	},
	data() {
		return {
			// 保存自定义事件的值 - 时间
			datePickerValues: {},
			// 保存自定义事件的值 - 文本
			singleStringValues: {},
			rangeStringValues: {}, // {rowValue___num1,rowValue___num2}
			// 选中的条件，和传入的项目相同
			selected:[],
			// TODO 默认值
			selected2: [
				{
					"label": "项目类型",
					"value": "projType",
					"isMultiple": true,
					"custom": false,
					"data": [{ "label": "房建工程", "value": "FJGC" }]
				}, {
					"label": "招标结束时间",
					"value": "endTime",
					"isMultiple": false,
					"custom": 'single', // 只有当单选时自定义才生效
					"data": [{ "label": "custom", "value": "1570550400000" }]
				},
			],

			time1: "",
			styleClass: {
				label: {
					'background': 'red'
				},
				// 选中条件的样式
				selected: {
					'background-color': '#FF552E'
				}
			},


		}
	}
}