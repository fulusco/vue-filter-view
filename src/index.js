import FilterView from './components/filterview/index.vue';
export default FilterView;
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.component('filter-view', FilterView);
}
